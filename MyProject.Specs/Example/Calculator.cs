﻿using System;
using System.Diagnostics;

namespace Example
{
    public class Calculator
    {
        public decimal FirstNumber { get; set; }
        public decimal SecondNumber { get; set; }

        public Calculator()
        {
        }

        public decimal Add()
        {
            return FirstNumber + SecondNumber;
        }
    }
}
